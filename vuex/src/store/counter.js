export default {
  state: {
    counter: 0
  },
  mutations: {
    // в mutations НЕЛЬЗЯ делать ассинхронные операции!!!
    changeCounter(state, payload){
      state.counter += payload
    }
  },
  actions: {
    // ТОЛЬКО в экшенах выполняем всю асинхронную бадягу
    // меняется все через свойство mutations (actions -> mutations), через функцию commit
    asyncChangeCounter(context, payload) {
      setTimeout(() => {
        context.commit('changeCounter', payload.counterValue)
      }, payload.timeDelay)
    }
  },
  getters: {
    computedCounter(state) {
      return state.counter * 3;
    },
    counter(state) {
      return state.counter
    }
  }
}