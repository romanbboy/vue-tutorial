import Vue from 'vue'
import App from './App.vue'
import ColorDirective from './color'
import List from './List'

export const eventEmitter = new Vue();

Vue.directive('colored', ColorDirective)

Vue.filter('addWord', val => val + ' newWord')

Vue.component('v-list', List)

new Vue({
  el: '#app',
  render: h => h(App)
})
