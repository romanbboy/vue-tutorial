// у директив тоже есть свои жизненные циклы

export default {
  bind(el, bindings, vnode) {
    console.log('bind');

    const arg = bindings.arg
    const modifiers = bindings.modifiers;

    let delay = 0;
    if(modifiers['delay']) delay = 2000;

    setTimeout(function(){
      el.style[arg] = bindings.value
    }, delay)
  },

  inserted(el, bindings, vnode) {
    console.log('inserted')
  },

  update(el, bindings, vnode, oldVnode) {
    console.log('update')
  },

  componentUpdated(el, bindings, vnode, oldVnode) {
    console.log('componentUpdated')
  },

  unbind() {
    console.log('unbind')
  }
}
