export default {
  data(){
    return {
      filmix: {
        title: 'Hello Roman',
        names: ['Zhenya', 'Roman', 'Vova', 'Igor'],
        searchName: ''
      }
    }
  },
  computed: {
    filteredNames() {
      return this.filmix.names.filter(name => {
        return name.toLowerCase().indexOf(this.filmix.searchName) !== -1
      })
    }
  },
}