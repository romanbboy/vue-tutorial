import VueRouter from 'vue-router'

import Home from './pages/Home'
import Car from './pages/Car'
import CarFull from './pages/CarFull'
import Error404 from './pages/Error404'

// chunks или ленивая подгрузка компонента
// в общий билд не складывается, подгружается по требованию
const Cars = resolve => {
  require.ensure(['./pages/Cars.vue'], () => resolve(require('./pages/Cars.vue')))
}

export default new VueRouter({
  routes: [
    {
      path: '',
      component: Home
    },
    {
      path: '/cars',
      component: Cars
    },
    {
      path: '/car/:id',
      component: Car,
      children: [
        {
          path: 'full',
          component: CarFull,
          name: 'CarFull',
          beforeEnter(to, from, next) {
            console.log('-----> ', 'beforeEnter');
            next()
          }
        }
      ]
    },
    {
      path: '/none',
      redirect: '/cars'
    },
    {
      path: '*',
      component: Error404
    }
  ],
  mode: 'history',  // убираем в адресной строке /#/
  scrollBehavior(to, from, savedPosition) {

    // открываем новую страницу, в скроле остаемся в том же месте
    if(savedPosition){
      return savedPosition
    }

    //если сохраненной позиции нет, то скролим к айдишнику через хэш
    if(to.hash){
      return {selector: to.hash}
    }

    // дефолтный скролл к месту, если нет хэша
    return {
      x: 0,
      y: 0
    }
  }
})