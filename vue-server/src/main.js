import Vue from 'vue'
import VueResource from 'vue-resource'

import App from './App.vue'

Vue.use(VueResource)

//глобальная настройка адреса для vue-resource
Vue.http.options.root = 'http://localhost:3000';

//это нужно что бы ВСЕ запросы на сервер уходили с добавленными заголовками, например токен
Vue.http.interceptors.push(request => {
  request.headers.set('Auth', 'Token' + Math.random())
})

new Vue({
  el: '#app',
  render: h => h(App)
})
